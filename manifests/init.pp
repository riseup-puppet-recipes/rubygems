#
# rubygems module
# original by luke kanies
# http://github.com/lak
#
# Copyright 2008, Puzzle ITC GmbH
# Marcel Härry haerry+puppet(at)puzzle.ch
# Simon Josi josi+puppet(at)puzzle.ch
#
# This program is free software; you can redistribute
# it and/or modify it under the terms of the GNU
# General Public License version 3 as published by
# the Free Software Foundation.
#

class rubygems {

  if $::operatingsystem != 'Debian' and $::operatingsystemmajrelease <= '9' {
    package{ 'rubygems': ensure => installed }
  }
  # this define allows us do declare local gems, like the following:
  # local_gem { "sqlite3-ruby":
  #  gem     => "sqlite3-ruby-1.2.1.gem",
  #  require => Package["sqlite-devel"]
  # }
  define local_gem($gem) {
    $dir = "/var/local/lib/gems" 
    $path = "$dir/$gem" 

    file {
      '/var/local/lib':
        ensure => directory,
        owner  => root,
        group  => 0,
        mode   => '0755';

      '/var/local/lib/gems':
        ensure => directory,
        owner  => root,
        group  => 0,
        mode   => '0755';

      $path:
        ensure  => present,
        source  => "puppet:///modules/rubygems/gems/${gem}",
        require => File[$dir],
        owner   => root,
        group   => 0,
        mode    => '0664'
    }

    package { $title:
      ensure   => installed,
      provider => 'gem',
      require  => File[$path],
      source   => $path
    }
  }

  file { '/etc/gemrc':
    source => [ 'puppet:///modules/site_rubygems/gemrc',
                'puppet:///modules/rubygems/gemrc' ],
    mode   => '0644',
    owner  => 'root',
    group  => 0,
  }

}
