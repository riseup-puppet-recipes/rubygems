class rubygems::highline {
  require rubygems

  case $::operatingsystem {
    debian,ubuntu: {
      case $::lsbdistcodename {
        'squeeze': {
          $package_name = 'ruby-highline'
          $provider = 'apt'
        }
        'wheezy': {
          $package_name = 'libhighline-ruby'
          $provider = 'apt'
        }
        default: {
          $package_name = 'ruby-highline'
          $provider = 'apt'
        }
      }
    }
    default: {
      $package_name = 'highline'
      $provider = 'gem'
    }
  }

  package { 'highline':
    ensure   => present,
    provider => $provider,
    name     => $package_name
  }
}
