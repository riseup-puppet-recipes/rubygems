class rubygems::moneta {
  require rubygems

  local_gem { "moneta":
    gem => "moneta-0.6.1.gem"
  }
}
